# Quiz Game

## Project description

Quiz service for solving prepared quizes and comparing quiz stats.

Quiz Page designed as the first part of the project [here](https://gitlab.com/avan1235/typescript-html-game) and then 
the server part of the project was written to integrate the server with served pages and keep the quiz data on server side.

App allows to solve quizes saved in server database and check quiz solutions statistics. Users has to be logged in in 
order to solve quizes.

## Used technologies

Project written in Typescript. Used frameworks:
* ExpressJS - application server framework for routing 
* SQLite - database connection in the application
* Selenium webdriver with Mocha and Chai - tests writing 

## Active demo

Active version of application is deployed on Heroku [here](https://elite-quiz.herokuapp.com/auth). In order to play the game 
user has to logged in to the service (for example with login: **user0** and password: **user0**).

### Screenshots

![login](./imgs/login_page.png)

![hello](./imgs/hello_screen.png)

![hello](./imgs/question_screen.png)

![hello](./imgs/results_screen.png)