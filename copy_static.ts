import { mkdir, cp } from 'shelljs';

mkdir('-p', 'dist/public/scripts/');

mkdir('-p', 'dist/public/fonts/');
cp('-R', 'src/public/fonts/*', 'dist/public/fonts/');
mkdir('-p', 'dist/public/img/');
cp('-R', 'src/public/img/*', 'dist/public/img/');