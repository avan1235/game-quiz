import express from 'express';
import dotenv from 'dotenv';
import { join } from 'path';
import cookieParser from 'cookie-parser';
import session from 'express-session';
import csurf from 'csurf';
import * as DB from './db';
import * as QuizData from './quiz';
import { echo } from 'shelljs';
import { Database } from 'sqlite3';
import { MemoryStore } from 'express-session';
import { promisify } from 'util';

const DEFAULT_PORT = 8080;
const DEFAULT_SECRET_KEY = 'University of Warsaw';
const DEFAULT_USE_HTTPS = false;

const csrfProtect = process.env.NODE_ENV === 'test'
                    ? csurf({ ignoreMethods: ['GET', 'HEAD', 'OPTIONS', 'POST', 'PUT'] })
                    : csurf({ cookie: true });
const store = new MemoryStore();

const checkUserLogged = (req: express.Request<any>, res: express.Response<any>, next: express.NextFunction) => {
    if (!isLoggedUser(req)) {
        res.redirect('/auth');
        return;
    }
    next();
};

const checkUserNotLogged = (req: express.Request<any>, res: express.Response<any>, next: express.NextFunction) => {
    if (isLoggedUser(req)) {
        res.redirect('/');
        return;
    }
    next();
};

const app = express();
dotenv.config();

const SECRET_KEY: string = process.env.SECRET_KEY || DEFAULT_SECRET_KEY;
const USE_HTTPS = process.env.USE_HTTPS !== undefined ? process.env.USE_HTTPS === 'true' : DEFAULT_USE_HTTPS;

// Express configuration
app.set('view engine', 'pug');
app.set('port', process.env.PORT || DEFAULT_PORT);

if (USE_HTTPS) {
    app.set('trust proxy', 1);
}

app.use(sslRedirect());
app.use('/static/', express.static(join(__dirname, 'public')));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cookieParser(SECRET_KEY));
app.use(session({
    secret: SECRET_KEY,
    resave: false,
    saveUninitialized: false,
    cookie: {
        secure: USE_HTTPS
    },
    store
}));
app.use(csrfProtect);

// Primary app routes.
app.get('/', checkUserLogged, (req, res) => {
    res.render('game', { csrfToken: req.csrfToken() });
});

app.get('/quiz', checkUserLogged, async (req, res) => {
    const connection = DB.getConnectionForRO();
    try {
        const quizes = await DB.getAllQuizes(connection);
        connection.close();
        res.json(quizes);
    }
    catch (err) {
        handleError(err, '/quiz', res, req, connection);
    }
});

app.get('/quiz/:quizId(\\d+)', checkUserLogged, async (req, res) => {
    const connection = DB.getConnectionForRO();
    const quizId = parseInt(req.params.quizId, 10);
    const username = req.session.user;
    try {
        const quiz = await DB.getFullQuiz(quizId, connection, false);
        if (await isQuizStartedByUser(username, quizId)) {
            showError(res, req, 'Quiz started but not canceled');
            return;
        }
        req.session.startTime[quizId] = Date.now();
        res.json(quiz);
    }
    catch (err) {
        handleError(err, '/quiz/:quizId [GET]', res, req, connection);
    }
});

app.post('/quiz/:quizId(\\d+)', checkUserLogged, async (req, res) => {
    const connection = DB.getConnectionForRW();
    const parsedQuizId = parseInt(req.params.quizId, 10);
    try {
        const quiz = await DB.getFullQuiz(parsedQuizId, connection, true) as QuizData.QuizFullWithAnswers;
        const username = req.session.user;
        const time = req.session.startTime[parsedQuizId];
        const result = QuizData.checkQuizAnswers(quiz, req.body, time, username);
        if (result === undefined) {
            showError(res, req, 'Invalid quiz result.');
            connection.close();
            return;
        }
        const [toSend, answers, dbStats] = result;
        await DB.saveUserFullStats(dbStats, answers, connection);
        res.json(toSend);
        connection.close();
    }
    catch (err) {
        handleError(err, 'Quiz already solved', res, req, connection);
    }
});

app.get('/quiz/:quizId(\\d+)/stats', checkUserLogged, async (req, res) => {
    const quizId = parseInt(req.params.quizId, 10);
    const username = req.session.user;
    const connection = DB.getConnectionForRO();
    try {
        const historyStats = await DB.getFullQuizHistoryStats(quizId, username, connection);
        connection.close();
        res.json(historyStats);
    }
    catch (err) {
        handleError(err, '/quiz/:quizId/stats [GET]', res, req, connection);
    }
});

app.get('/quiz/:quizId(\\d+)/cancel', checkUserLogged, async (req, res) => {
    const quizId = parseInt(req.params.quizId, 10);
    const username = req.session.user;
    try {
        if (!await isQuizStartedByUser(username, quizId)) {
            showError(res, req, 'Quiz not started so cannot be canceled');
            return;
        }
        await cancelQuizStartedByUser(username, quizId);
        res.statusCode = 200;
        res.json({
            message: "Canceled"
        });
    }
    catch (err) {
        handleError(err, '/quiz/:quizId/stats [GET]', res, req);
    }
});

app.get('/auth', checkUserNotLogged, (req, res) => {
    res.render('login', { csrfToken: req.csrfToken() });
});

app.get('/reset', checkUserNotLogged, (req, res) => {
    res.render('reset', { csrfToken: req.csrfToken() });
});

app.get('/logout', checkUserLogged, async (req, res) => {
    try {
        await destroySession(req);
        res.redirect('/');
    }
    catch (err) {
        handleError(err, '/logout', res, req);
    }
});

app.post('/auth', async (req, res) => {
    const username = req.body.username;
    const password = req.body.password;

    const connection = DB.getConnectionForRW();
    try {
        const result = await DB.validateUser(username, password, connection);
        if (!result) {
            showError(res, req, 'Invalid password');
            connection.close();
            return;
        }
        await regenerateSession(req, username);
        connection.close();
        res.redirect('/');
    } catch(err) {
        handleError(err, '/auth', res, req, connection);
    }
});

app.post('/reset', async (req, res) => {
    const username = req.body.username;
    const password = req.body.password;
    const resetPassword = req.body.reset;

    const connection = DB.getConnectionForRW();
    try {
        const result = await DB.validateAndUpdateUser(username, password, resetPassword, connection);
        if (!result) {
            showError(res, req, 'Invalid data');
            connection.close();
            return;
        }
        await destroySession(req, username);
        connection.close();
        res.redirect('/auth');
    } catch(err) {
        handleError(err, '/reset', res, req, connection);
    }
});

app.get('/error/alreadySolved', (req, res) => {
    showError(res, req, 'Quiz already solved', false, true);
});

app.get('/error', (req, res) => {
    showError(res, req, 'Unknown error', true);
})

app.all('*', (req, res) => {
    showError(res, req, 'Page not found');
});


// Helper functions

function handleError(err: Error, path: string, res: express.Response<any>, req: express.Request<any>, connectionToClose?: Database): void {
    echo(`Error on ${path}:`);
    echo(`${err}`);
    if (connectionToClose !== undefined && connectionToClose !== null) {
        connectionToClose.close();
    }
    showError(res, req, 'Something bad happen');
}

function showError(res: express.Response<any>, req: express.Request<any>, descriptionText?: string, working: boolean = false, redirectShow: boolean = false): void {
    res.status(404);
    res.render('error', {
        description: descriptionText,
        showWorking: working,
        csrfToken: req.csrfToken(),
        showRedirect: redirectShow
    });
}

function isLoggedUser(req: express.Request<any>): boolean {
    return req.session.user;
}

function sslRedirect(): express.RequestHandler  {
    return (req: express.Request<any>, res: express.Response<any>, next: express.NextFunction) => {
        if (USE_HTTPS) {
            if (req.headers['x-forwarded-proto'] !== 'https') {
                res.redirect(302, 'https://' + req.hostname + req.originalUrl);
            }
            else {
                next();
            }
        }
        else {
            next();
        }
    };
}

function destroySession(req: express.Request<any>, username: string = null): Promise<void> {
    return new Promise((resolve, reject) => {
        req.session.destroy(error => {
            if (error) {
                reject(error);
            }
            if (username === null) {
                resolve();
                return;
            }
            store.all((err: any, sessions: any) => {
                if (err) {
                    reject(err);
                }
                const result = Object.entries(sessions)
                    .filter(([_k, v]: [any, any]) => v.user === username)
                    .map(([k, _v]) => promisify(store.destroy.bind(store))(k));
                Promise.all(result).then(() => {
                    resolve();
                }, e => reject(e)).catch(e => reject(e));
            });
        });
    });
}

function isQuizStartedByUser(username: string, quizId: number): Promise<boolean> {
    return new Promise((resolve, reject) => {
        store.all((err: any, sessions: any) => {
            if (err) {
                reject(err);
            }
            const result = Object.entries(sessions)
                .filter(([_k, v]: [any, any]) => v.user === username)
                .filter(([_k, v]: [any, any]) => {
                    return v.startTime[quizId] !== undefined;
                }).length;
            resolve(result !== 0);
        });
    });
}

function cancelQuizStartedByUser(username: string, quizId: number): Promise<void> {
    return new Promise((resolve, reject) => {
        store.all((err: any, sessions: any) => {
            if (err) {
                reject(err);
            }
            Object.entries(sessions)
                .filter(([_k, v]: [any, any]) => v.user === username)
                .forEach(([k, v]: [any, any]) => {
                    v.startTime[quizId] = undefined;
                    store.set(k, v);
                });
            resolve();
        });
    });
}

function regenerateSession(req: express.Request<any>, username: string): Promise<void> {
    return new Promise((resolve, reject) => {
        req.session.regenerate(err => {
            if (err) {
                reject(err);
                return;
            }
            req.session.startTime = {};
            req.session.user = username;
            resolve();
        });
    });
}

export default app;