import * as db from 'sqlite3';
import { exit, echo } from 'shelljs';
import * as crypt from 'bcrypt';
import * as QuizData from './quiz';

export const NAME = 'game-database.sqlite';
const SALT_ROUNDS = 10;

export { Database } from 'sqlite3';

export function getConnectionForRW(): db.Database {
    return new db.Database(NAME, (err: Error) => {
        if (err) {
            echo('Connection to RW not estabilished, Exiting...');
            exit(1);
        }
    });
};

export function getConnectionForRO(): db.Database {
    return new db.Database(NAME, db.OPEN_READONLY, (err: Error) => {
        if (err) {
            echo('Connection to RO not estabilished, Exiting...');
            exit(1);
        }
    });
};

interface UserDB {
    username: string;
    password_hash: string;
}

interface QuizDB {
    id: number;
    title: string;
    description: string;
}

interface QuestionDB {
    id: number;
    title: string;
    body: string;
    quiz_id: number;
    correct_answer_value: number;
    bad_answer_penalty_time: number;
}

interface AnswerDB {
    question_id: number;
    answer_value: number;
}

export interface UserQuizStatisticsDB {
    quiz_id: number;
    played_by: string,
    full_time: number;
}

export interface UserAnswerDB {
    question_id: number;
    answer_value: number;
    answered_by: string;
    answer_seconds: number;
}

export function reset(dbForRW: db.Database): Promise<void> {
    return new Promise((resolve, reject) => {
        dbForRW.exec(`
        drop table if exists users_answer;
        drop table if exists users_quiz_stat;
        drop table if exists answer;
        drop table if exists question;
        drop table if exists quiz;
        drop table if exists user;
        create table user (
            username text primary key,
            password_hash text not null
        );
        create table quiz (
            id integer primary key,
            title text not null,
            description text not null
        );
        create table question (
            id integer primary key,
            title text not null,
            body text not null,
            quiz_id integer not null,
            correct_answer_value integer not null,
            bad_answer_penalty_time integer not null,
            foreign key(quiz_id) references quiz(id)
        );
        create table answer (
            question_id integer not null,
            answer_value integer not null,
            primary key(question_id, answer_value),
            foreign key(question_id) references question(id)
        );
        create table users_quiz_stat (
            quiz_id number not null,
            played_by text not null,
            full_time integer not null,
            primary key(quiz_id, played_by),
            foreign key(quiz_id) references quiz(id),
            foreign key(played_by) references user(username)
        );
        create table users_answer (
            question_id integer not null,
            answer_value integer not null,
            answered_by text not null,
            answer_seconds integer not null,
            primary key(question_id, answered_by),
            foreign key(question_id) references question(id),
            foreign key(answered_by) references user(username)
        );
        `, (err: Error) => {
            if (err) {
                reject(err);
                return;
            }
            resolve();
        });
    });
}

export function addUser(username: string, password: string, dbForRW: db.Database): Promise<void> {
    return new Promise((resolve, reject) => {
        crypt.hash(password, SALT_ROUNDS, (err, encrypted) => {
            if (err) {
                reject(err);
                return;
            }
            dbForRW.run(`
            insert into user
            values (?, ?);
            `, [username, encrypted], (errDb: Error) => {
                if (errDb) {
                    reject(errDb);
                    return;
                }
                resolve();
            });
        });
    });
}

export function validateAndUpdateUser(username: string, oldPassword: string, newPassword: string, dbForRW: db.Database): Promise<boolean> {
    return new Promise((resolve, reject) => {
        dbForRW.get(`
        select * from user
        where username = ?;
        `, [username], (err: Error, hashed: UserDB) => {
            if (err) {
                reject(err);
                return;
            }
            if (hashed === undefined) {
                reject(Error('No user with specified username'));
                return;
            }
            crypt.compare(oldPassword, hashed.password_hash,
                (errCrypt: Error, same: boolean) => {
                    if (errCrypt) {
                        reject(errCrypt);
                        return;
                    }
                    if (same) {
                        crypt.hash(newPassword, SALT_ROUNDS, (errIn, encrypted) => {
                            if (errIn) {
                                reject(errIn);
                                return;
                            }
                            dbForRW.run(`
                            update user
                            set password_hash = ?
                            where username = ?;
                            `, [encrypted, username], (errDb: Error) => {
                                if (errDb) {
                                    reject(errDb);
                                    return;
                                }
                                resolve(true);
                            });
                        });
                    }
                    else {
                        reject(Error(`Passwords don't match`));
                    }
            });
        });
    });
}

function saveUserStats(stats: UserQuizStatisticsDB, dbForRW: db.Database): Promise<void> {
    return new Promise((resolve, reject) => {
        dbForRW.run(`
        insert into users_quiz_stat
        values (?, ?, ?);
        `, [stats.quiz_id, stats.played_by, stats.full_time],
        (err: Error) => {
            if (err) {
                reject(err);
                return;
            }
            resolve();
        });
    });
}

function saveUserAnswers(answers: UserAnswerDB[], dbForRW: db.Database): Promise<void[]> {
    const allInserts: any[] = [];
    for (const answer of answers) {
        const single = new Promise((resolve, reject) => {
            dbForRW.run(`
            insert into users_answer
            values (?, ?, ?, ?);
            `, [answer.question_id, answer.answer_value, answer.answered_by, answer.answer_seconds],
            (err: Error) => {
                if (err) {
                    reject(err);
                    return;
                }
                resolve();
            });
        });
        allInserts.push(single);
    }
    return Promise.all(allInserts);
}

export function saveUserFullStats(stats: UserQuizStatisticsDB, answers: UserAnswerDB[], dbForRW: db.Database): Promise<void> {
    return saveUserAnswers(answers, dbForRW).then(() => saveUserStats(stats, dbForRW));
}

export function validateUser(username: string, password: string, dbForRO: db.Database): Promise<boolean> {
    return new Promise((resolve, reject) => {
        dbForRO.get(`
        select * from user
        where username = ?;
        `, [username], (err: Error, hashed: UserDB) => {
            if (err) {
                reject(err);
                return;
            }
            if (hashed === undefined) {
                reject(Error('No user with specified username'));
                return;
            }
            crypt.compare(password, hashed.password_hash,
                (errCrypt: Error, same: boolean) => {
                    if (errCrypt) {
                        reject(errCrypt);
                        return;
                    }
                    resolve(same);
            });
        });
    });
}

function addBestUsersTimesOfQuiz(stats: QuizData.QuizHistoryStats, id: number, limit: number, dbForRO: db.Database): Promise<void> {
    return new Promise((resolve, reject) => {
        dbForRO.all(`
        select quiz_id, played_by, full_time
        from users_quiz_stat
        where quiz_id = ?
        order by full_time asc
        limit ?;
        `, [id, limit], (err: Error, rows) => {
            if (err) {
                reject(err);
                return;
            }
            rows.forEach(r => {
                const userResult: QuizData.BaseQuizHistoryStatistics = {
                    username: r.played_by,
                    fullTime: r.full_time,
                    isAnswerValid: []
                };
                stats.bestUsersStats.push(userResult);
            });
            resolve();
        });
    });
}

function addBestUsersResultsToStats(stats: QuizData.QuizHistoryStats, id: number, dbForRO: db.Database): Promise<void[]> {
    if (stats.bestUsersStats.length === 0) {
        return;
    }
    const result: Promise<void>[] = [];
    for (const user of stats.bestUsersStats) {
        const single = addUserAnswersToStats(user, id, user.username, dbForRO);
        result.push(single);
    }
    return Promise.all(result);
}

function addUserAnswersToStats(stats: QuizData.UserQuizHistoryStatistics | QuizData.BaseQuizHistoryStatistics, quizId: number, username: string, dbForRO: db.Database): Promise<void> {
    return new Promise((resolve, reject) => {
        dbForRO.all(`
        select correct_answer_value, answer_value
        from (select id, correct_answer_value from question where quiz_id = ?)
        left join (select * from users_answer where answered_by = ?) on id = question_id
        order by id;
        `, [quizId, username], (err: Error, rows) => {
            if (err) {
                reject(err);
                return;
            }
            if (rows.some(r => r.answer_value === null)) {
                // no answers from user
                resolve();
                return;
            }
            stats.username = username;
            stats.isAnswerValid = rows.map(r => r.correct_answer_value === r.answer_value);
            if (QuizData.isUserStats(stats)) {
                stats.validAnswer = rows.map(r => r.correct_answer_value);
            }
            resolve();
        });
    });
}

function addAvgTimesToStats(stats: QuizData.UserQuizHistoryStatistics, quizId: number, dbForRO: db.Database): Promise<void> {
    return new Promise((resolve, reject) => {
        if (stats.username === null) {
            // no need to add correct anser to stats with no data
            resolve();
            return;
        }
        dbForRO.all(`
        select id, ifnull(avg(answer_seconds), 0) as avg
        from (select id, correct_answer_value from question where quiz_id = ?)
        left join users_answer on (id = question_id and answer_value = correct_answer_value)
        where id is not null
        group by id
        order by id;
        `, [quizId], (err: Error, rows) => {
            if (err) {
                reject(err);
                return;
            }
            stats.answerAvgTime = rows.map(r => r.avg);
            resolve();
        });
    });
}

function addUserTimeToStats(stats: QuizData.UserQuizHistoryStatistics, quizId: number, user: string, dbForRO: db.Database): Promise<void> {
    return new Promise((resolve, reject) => {
        if (stats.username === null) {
            // no need to add correct anser to stats with no data
            resolve();
            return;
        }
        dbForRO.get(`
        select full_time from users_quiz_stat
        where played_by = ? and quiz_id = ?
        `, [user, quizId], (err: Error, row) => {
            if (err) {
                reject(err);
                return;
            }
            stats.fullTime = row.full_time;
            resolve();
        });
    });
}

function getUserQuizHistoryStats(id: number, user: string, dbForRO: db.Database): Promise<QuizData.UserQuizHistoryStatistics> {
    const historyStats: QuizData.UserQuizHistoryStatistics = {
        username: null,
        fullTime: null,
        validAnswer: [],
        isAnswerValid: [],
        answerAvgTime: []
    }

    return addUserAnswersToStats(historyStats, id, user, dbForRO).then(
        () => addAvgTimesToStats(historyStats, id, dbForRO)).then(
        () => addUserTimeToStats(historyStats, id, user, dbForRO)).then(
        () => {
            if (historyStats.username === null) {
                return null;
            }
            return historyStats;
        });
}

export async function getFullQuizHistoryStats(id: number, user: string, dbForRO: db.Database, limit: number = 10): Promise<QuizData.QuizHistoryStats> {
    const fullStats: QuizData.QuizHistoryStats = {
        userStats: null,
        bestUsersStats: []
    }
    fullStats.userStats = await getUserQuizHistoryStats(id, user, dbForRO);
    await addBestUsersTimesOfQuiz(fullStats, id, limit, dbForRO);
    await addBestUsersResultsToStats(fullStats, id, dbForRO);
    return fullStats;
}

export function getAllQuizes(dbForRO: db.Database): Promise<QuizData.Quiz[]> {
    return new Promise((resolve, reject) => {
        dbForRO.all(`
        select * from quiz;
        `, (err: Error, data: QuizDB[]) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(data.map(quiz => {
                return {
                    id: quiz.id,
                    title: quiz.title
                }
            }));
        });
    });
}

function getAllQuizQuestions(quizId: number, dbForRO: db.Database): Promise<QuizData.QuestionFull[]> {
    return new Promise((resolve, reject) => {
        dbForRO.all(`
        select * from question
        where quiz_id = ?
        order by id;
        `, [quizId], (err: Error, data: QuestionDB[]) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(data.map(q => {
                return {
                    id: q.id,
                    title: q.title,
                    body: q.body,
                    correctAnswerValue: q.correct_answer_value,
                    possibleAnswers: [],
                    badAnswerPenaltyTime: q.bad_answer_penalty_time
                }
            }));
        });
    });
}

function addPossibleAnswersToQuestion(question: QuizData.QuestionFull, dbForRO: db.Database): Promise<QuizData.QuestionFull> {
    return new Promise((resolve, reject) => {
        dbForRO.all(`
        select * from answer
        where question_id = ?;
        `, [question.id], (err: Error, answers: AnswerDB[]) => {
            if (err) {
                reject(err);
                return;
            }
            question.possibleAnswers = answers.map(a => a.answer_value);
            resolve(question);
        });
    });
}

function getQuizQuestionsWithPossibleAnswers(quizId: number, dbForRO: db.Database): Promise<QuizData.QuestionFull[]> {
    return getAllQuizQuestions(quizId, dbForRO).then(questions => {
        const waiting: Promise<QuizData.QuestionFull>[] = []
        questions.forEach(q => {
            const single = addPossibleAnswersToQuestion(q, dbForRO);
            waiting.push(single);
        });
        return Promise.all(waiting);
    });
}

function addQuestionsToQuiz(quizData: QuizData.QuizFull, dbForRO: db.Database, withAnswers: boolean): Promise<QuizData.QuizFull | QuizData.QuizFullWithAnswers> {
    return getQuizQuestionsWithPossibleAnswers(quizData.id, dbForRO).then(questions => {
        if (withAnswers) {
            quizData.questions = questions;
        }
        else {
            quizData.questions = questions.map(q => {
                delete q.correctAnswerValue
                return q as QuizData.Question;
            });
        }
        return quizData;
    })
}

export function getFullQuiz(quizId: number, dbForRO: db.Database, withAnswers: boolean = false): Promise<QuizData.QuizFull | QuizData.QuizFullWithAnswers> {
    return new Promise((resolve, reject) => {
        dbForRO.get(`
            select * from quiz
            where id = ?;
        `, [quizId], (err: Error, quiz: QuizDB) => {
            if (err) {
                reject(err);
                return;
            }
            if (quiz === undefined) {
                reject(Error('Quiz not exits'));
                return;
            }
            const quizData: QuizData.QuizFull = {
                id: quiz.id,
                title: quiz.title,
                description: quiz.description,
                questions: []
            };
            resolve(addQuestionsToQuiz(quizData, dbForRO, withAnswers));
        });
    });
}

export function addQuiz(quiz: QuizData.QuizFullWithAnswers, dbForRW: db.Database): Promise<void> {
    const quizInsert = `
        insert or rollback into quiz (id, title, description)
        values (${quiz.id}, '${prepare(quiz.title)}', '${prepare(quiz.description)}');
    `;
    const questionsInserts = quiz.questions.map(question => {
        return buildQueryForQuestionInsert(question, quiz.id);
    }).join("");
    const fullTransaction = `
        begin;
        ${quizInsert}
        ${questionsInserts}
        commit;
    `;
    return new Promise((resolve, reject) => {
        dbForRW.exec(fullTransaction, (err: Error) => {
            if (err) {
                reject(err);
                return;
            }
            resolve();
        });
    });
}

function buildQueryForQuestionInsert(question: QuizData.QuestionFull, quizId: number): string {
    const queryBegin = `
        insert or rollback into question (id, title, body, quiz_id, correct_answer_value, bad_answer_penalty_time)
        values (${question.id}, '${prepare(question.title)}', '${prepare(question.body)}', ${quizId}, ${question.correctAnswerValue}, ${question.badAnswerPenaltyTime});
    `;
    const answersInserts = question.possibleAnswers.map(value => {
        return `
            insert or rollback into answer (question_id, answer_value)
            values (${question.id}, ${value});
        `;
    }).join("");
    return queryBegin + answersInserts;
}

function prepare(value: string): string {
    return value.replace(/\'/g,"''");
}

export function exec(statement: string, dbForRO: db.Database): Promise<void> {
    return new Promise((resolve, reject) => {
        dbForRO.exec(statement, (err: Error) => {
            if (err) {
                reject(err);
                return;
            }
            resolve();
        })
    });
}
