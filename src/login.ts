import * as Util from './utils.js';

const nameInput = Util.queryDiv('#nameInput') as HTMLInputElement;
const passwordInput = Util.queryDiv('#passwordInput') as HTMLInputElement;
const newPasswordInput = Util.queryDiv('#newPasswordInput') as HTMLInputElement;

Util.addInputListener(nameInput);
Util.addInputListener(passwordInput);
Util.addInputListener(newPasswordInput);