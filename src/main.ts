import * as Quiz from './quiz.js';
import * as Time from './time.js';
import * as Div from './divs.js';
import * as Util from './utils.js';

// CONSTANT DIV WITH ID

const STATS_WIDTH_CHANGE = 1200;
const COUNT_BEST_ON_BOARD = 10;

const CSRF_TOKEN = Util.queryDiv('meta[name="csrf-token"]').getAttribute('content');

// GLOBAL VARIABLES

let CURRENT_QUIZ: Quiz.QuizFull = null;
let MAX_QUESTION_ID: number = null;
let ANSWERS: number[] = null;
let COUNT_TIMES: Quiz.CountTime[] = null;
let STEP_BOX_LISTENERS: (() => void)[] = null; // to get it easily created, removed and restored during animations
let ROTATIONS = 0;

// FUNCTIONS DEFINITIONS

function getStepBoxById(id: number): HTMLDivElement {
    return Util.queryDiv(`.stepBox[stepId=\'${id}\']`);
}

function getAllStepBox(): NodeListOf<HTMLDivElement> {
    return document.querySelectorAll('.stepBox');
}

function markStepBoxAsCurrent(stepId: number) {
    const curQuestion = getCurrentQuestion();
    const curId = getQuestionId(curQuestion);
    if (curId === stepId || stepId < 0 || stepId > MAX_QUESTION_ID) {
        return;
    }
    const curStepBox = Util.queryDiv('.stepBox[current]');
    const nextStepBox = getStepBoxById(stepId);

    curStepBox.removeAttribute('current');
    nextStepBox.setAttribute('current', '');
}

function setStepBoxMarked(stepId: number) {
    const stepBox = getStepBoxById(stepId);
    stepBox.setAttribute('marked', '');
}

function setStepBoxUnmarked(stepId: number) {
    const stepBox = getStepBoxById(stepId);
    stepBox.removeAttribute('marked');
}

function getCurrentQuestion(): HTMLDivElement {
    return Util.queryDiv('.singleQuestion[current]');
}

function getQuestionId(question: HTMLDivElement): number {
    return parseInt(question.getAttribute('questionId'), 10);
}

function getCurrentQuestionId(): number {
    return getQuestionId(getCurrentQuestion());
}

function getQuestionById(id: number): HTMLDivElement {
    return Util.queryDiv(`.singleQuestion[questionId=\'${id}\']`);
}

function isQuizGameFinished(): boolean {
    return ANSWERS.every(el => el !== null);
}

function manageFinishButtonVisibility(): void {
    if (isQuizGameFinished()) {
        Div.finish.removeAttribute('hidden');
    }
    else {
        Div.finish.setAttribute('hidden', '');
    }
}

function updateGameTime(timeValue: Time.TimeValue): void {
    Div.time.textContent = `Game time: ${Time.format(timeValue)}`;
}

function manageControlsVisibility(): void {
    const curQuestion = getCurrentQuestion();
    const curQuestionId = getQuestionId(curQuestion);

    if (curQuestionId === 0) {
        Div.prevButton.style.opacity = '0';
        Time.waitHalfSecond().then(() => {
            Div.prevButton.style.visibility = 'hidden';
        });
    }
    else {
        Div.prevButton.style.visibility = 'visible';
        Div.prevButton.style.opacity = '1';
    }

    if (curQuestionId === MAX_QUESTION_ID) {
        Div.nextButton.style.opacity = '0';
        Time.waitHalfSecond().then(() => {
            Div.nextButton.style.visibility = 'hidden';
        });
    }
    else {
        Div.nextButton.style.visibility = 'visible';
        Div.nextButton.style.opacity = '1';
    }
}

function updatePenaltyTime(penTime: number): void {
    Div.penalty.textContent = `Penalty time: ${penTime}s`;
}

function showQuestion(questionId: number): Promise<void> {
    const curQuestion = getCurrentQuestion();
    const curId = getQuestionId(curQuestion);
    if (curId === questionId || questionId < 0 || questionId > MAX_QUESTION_ID) {
        return;
    }
    const nextQuestion = getQuestionById(questionId);

    if (questionId > curId) {
        nextQuestion.style.transform = 'translate3d(100%, 0, 0)';
    }
    else {
        nextQuestion.style.transform = 'translate3d(-100%, 0, 0)';
    }
    markStepBoxAsCurrent(questionId);
    updatePenaltyTime(CURRENT_QUIZ.questions[questionId].badAnswerPenaltyTime);
    Time.changeIncrementedTo(COUNT_TIMES[questionId]);

    // here is the moment where for a while in document are 2 'current' questions
    // just for animation purpose
    nextQuestion.setAttribute('current', '');

    if (questionId > curId) {
        curQuestion.style.transform = 'translate3d(-100%, 0, 0)';
    }
    else {
        curQuestion.style.transform = 'translate3d(100%, 0, 0)';
    }

    return Time.waitShort().then(() => {
        nextQuestion.style.transform = 'translate3d(0, 0, 0)';
     }).then(Time.waitHalfSecond).then(() => {
        curQuestion.removeAttribute('current');
        curQuestion.style.transform = 'translate3d(0, 0, 0)';
    }).then(manageControlsVisibility);
}

function showShiftedQuestion(shift: number): Promise<void> {
    const curQuestion = getCurrentQuestion();
    const shiftedId = getQuestionId(curQuestion) + shift;
    return showQuestion(shiftedId);
}

function showNextQuestion(): Promise<void> {
    return showShiftedQuestion(1);
}

function showPrevQuestion(): Promise<void> {
    return showShiftedQuestion(-1);
}

function focusCurrentQuestionInput(): void {
    const input = getCurrentQuestion().querySelector('.answerInputField');
    if (input !== null) {
        (input as HTMLInputElement).focus();
    }
}

function nextButtonListener(): void {
    removeControlsListeners();
    const curQuestionId = getQuestionId(getCurrentQuestion());
    if (curQuestionId !== MAX_QUESTION_ID) {
        showNextQuestion().then(addControlsListeners);
    }
    else {
        addControlsListeners();
    }
}

function prevButtonListener(): void {
    removeControlsListeners();
    const curQuestionId = getQuestionId(getCurrentQuestion());
    if (curQuestionId !== 0) {
        showPrevQuestion().then(addControlsListeners);
        document.addEventListener('keydown', keyboardListener);
    }
    else {
        addControlsListeners();
    }
}

function removeControlsListeners(): void {
    // disable clicks for animation time
    Div.nextButton.removeEventListener('click', nextButtonListener);
    Div.prevButton.removeEventListener('click', prevButtonListener);
    document.removeEventListener('keydown', keyboardListener);
}

function addControlsListeners(): void {
    Div.nextButton.addEventListener('click', nextButtonListener);
    Div.prevButton.addEventListener('click', prevButtonListener);
    document.addEventListener('keydown', keyboardListener);
}

function removeStepBoxListeners(): void {
    document.querySelectorAll('.stepBox').forEach(box => {
        const stepId = parseInt(box.getAttribute('stepId'), 10);
        box.removeEventListener('click', STEP_BOX_LISTENERS[stepId]);
    });
}

function addStepBoxListeners(): void {
    document.querySelectorAll('.stepBox').forEach(box => {
        const stepId = parseInt(box.getAttribute('stepId'), 10);
        box.addEventListener('click', STEP_BOX_LISTENERS[stepId]);
    });
}

function keyboardListener(event: KeyboardEvent): void {
    switch(event.code) {
        case 'ArrowRight': {
            nextButtonListener();
        } break;
        case 'ArrowLeft': {
            prevButtonListener();
        } break;
        case 'ArrowDown': {
            focusCurrentQuestionInput();
        } break;
        case 'Enter': {
            if (isQuizGameFinished()) {
                goToGameStats();
            }
        } break;
    }
}

function manageStepBoxOverflow(): void {
    if (Util.isWidthOverflown(Div.progress)) {
        Div.progress.setAttribute('overflown', '');
    }
    else {
        Div.progress.removeAttribute('overflow');
    }
}

function getQuestionAnswers(questionId: number): NodeListOf<HTMLDivElement> {
    return document.querySelectorAll(`.singleQuestion[questionId=\'${questionId}\'] .singleAnswer`);
}

function addSingleAnswersListeners(): void {
    document.querySelectorAll('.singleQuestion .singleAnswer').forEach(answer => {
        answer.addEventListener('click', () => {
            const question = answer.closest('.singleQuestion') as HTMLDivElement;
            const questionId = getQuestionId(question);
            const curAnswer = parseInt(answer.textContent, 10);
            if (ANSWERS[questionId] === null) {
                answer.setAttribute('selected', '');
                ANSWERS[questionId] = curAnswer;
                setStepBoxMarked(questionId);
            }
            else if (ANSWERS[questionId] === curAnswer) {
                answer.removeAttribute('selected');
                ANSWERS[questionId] = null;
                setStepBoxUnmarked(questionId);
            }
            else {
                getQuestionAnswers(questionId).forEach(singleAnswer => {
                    singleAnswer.removeAttribute('selected');
                })
                answer.setAttribute('selected', '');
                ANSWERS[questionId] = curAnswer;
                setStepBoxMarked(questionId);
            }
            manageFinishButtonVisibility();
        });
    });
}

function isValidNumber(input: string): boolean {
    return new RegExp('^(\-|(\-)?[1-9][0-9]*|0)?$').test(input);
}

function addInputFilteredListener(input: HTMLInputElement): void {
    Util.addInputListener(input);

    ['input', 'keydown', 'keyup', 'mousedown', 'mouseup', 'select', 'contextmenu', 'drop'].forEach(event => {
        input.addEventListener(event, function () {
            if (isValidNumber(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            }
            else if (this.hasOwnProperty('oldValue')) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            }
            else {
                this.value = '';
            }

            const inVal = this.value;
            const answer = (inVal === '' || inVal === '-') ? null : parseInt(inVal, 10);
            const question = input.closest('.singleQuestion') as HTMLDivElement;
            const questionId = getQuestionId(question);
            ANSWERS[questionId] = answer;
            if (answer == null) {
                setStepBoxUnmarked(questionId);
            }
            else {
                setStepBoxMarked(questionId);
            }
            manageFinishButtonVisibility();
        });
      });
}

function addInputAnswersListeners() {
    document.querySelectorAll('.singleQuestion .answerInputField').forEach(input => {
        addInputFilteredListener(input as HTMLInputElement);
    });
}

function toggleDescription(): void {
    if (!Div.sideContainer.hasAttribute('hidden')) {
        Div.sideContainer.setAttribute('hidden', '');
    }
    else {
        Div.sideContainer.removeAttribute('hidden');
    }
}

function createSingleQuestion(question: Quiz.Question, id: number): HTMLDivElement {
    const titleQ = Util.createDivWithClasses('questionTitle', question.title);
    const body = Util.createDivWithClasses('question', question.body);

    const answersQ = Util.createDivWithClasses('answersBody');
    if (question.possibleAnswers.length > 0) {
        for (const ans of question.possibleAnswers) {
            const single = Util.createDivWithClasses('singleAnswer clickable', `${ans}`);
            answersQ.appendChild(single);
        }
    }
    else {
        const inputContainer = Util.createDivWithClasses('inputContainer');
        const inputLabel = Util.createDivWithClasses('inputLabel', 'Your answer');
        inputContainer.appendChild(inputLabel);

        const answerInputField = document.createElement('input');
        answerInputField.classList.add('answerInputField');
        answerInputField.setAttribute('autocomplete', 'off');
        inputContainer.appendChild(answerInputField);

        answersQ.appendChild(inputContainer);
    }

    const questionBody = document.createElement('questionBody');
    questionBody.classList.add('questionBody');
    questionBody.appendChild(titleQ);
    questionBody.appendChild(body);
    questionBody.appendChild(answersQ);

    const questionBox = Util.createDivWithClasses('questionBox');
    questionBox.appendChild(questionBody);

    const singleQuestion = Util.createDivWithClasses('singleQuestion');
    singleQuestion.setAttribute('questionId', `${id}`);
    singleQuestion.appendChild(questionBox);

    if (id === 0) {
        singleQuestion.setAttribute('current', '');
    }

    return singleQuestion;
}

function buildQuizWeb(quizData: Quiz.QuizFull): void {
    Div.title.textContent = quizData.title;
    Util.removeChildrenOf(Div.description);
    for (const par of quizData.description.split('\n')) {
        const p = document.createElement('p');
        p.textContent = par;
        Div.description.appendChild(p);
    }
    Util.removeChildrenOf(Div.progress);
    Util.removeChildrenOf(Div.questions);
    for (let i = 0; i < quizData.questions.length; i++) {
        const stepBox = Util.createDivWithClasses('stepBox clickable', `${i + 1}`);
        stepBox.setAttribute('stepId', `${i}`);
        if (i === 0) {
            stepBox.setAttribute('current', '');
        }
        Div.progress.appendChild(stepBox);
        Div.questions.appendChild(createSingleQuestion(quizData.questions[i], i));
    }

    MAX_QUESTION_ID = quizData.questions.length - 1;
    ANSWERS = Array.from({length: quizData.questions.length}, () => null);
    COUNT_TIMES = Array.from({length: quizData.questions.length}, () => {
        return { seconds: 0 }
    });

    // we keep them as the  global variable to get the ability to remove/add them
    STEP_BOX_LISTENERS = Array.from(getAllStepBox(), box => () => {
        removeStepBoxListeners(); // disable click for animation time
        const stepId = parseInt(box.getAttribute('stepId'), 10);
        const questionId = getCurrentQuestionId();
        if (questionId === stepId) {
            addStepBoxListeners();
        }
        else {
            showQuestion(stepId).then(addStepBoxListeners);
        }
    });

    updatePenaltyTime(CURRENT_QUIZ.questions[0].badAnswerPenaltyTime);
    Time.changeIncrementedTo(COUNT_TIMES[0]);
    manageControlsVisibility();
    addControlsListeners();
    addStepBoxListeners();
    addSingleAnswersListeners();
    manageFinishButtonVisibility();
    addInputAnswersListeners();
    Div.sideContainer.setAttribute('hidden', '');
    Time.waitSecond().then(() => {
        Time.resetAndRun(updateGameTime);
        Time.waitSecond().then(Time.waitQuarterSecond).then(manageStepBoxOverflow);
    });
}

// HELLO SITE

function setupBegin(): void {
    Div.gameContainer.style.display = 'none';
    Div.gameContainer.style.opacity = '0.0';
    Div.gameContainer.style.transform = 'scale(0.5)';

    Div.resultsContainer.style.display = 'none';
    Div.shadow.setAttribute('hidden', '');
    Div.resultsBoard.setAttribute('hidden', '');

    Util.queryDiv('#statsTitle').textContent = `Top ${COUNT_BEST_ON_BOARD} Results`;

    createQuizList();
}

function animateFlexContainers(from: HTMLDivElement, to: HTMLDivElement): void {
    from.style.transform = 'scale(0.5)';
    from.style.opacity = '0.0';
    Time.waitQuarterSecond().then(() => {
        from.style.display = 'none';
        to.style.display = 'flex';
    }).then(Time.waitQuarterSecond).then(() => {
        to.style.opacity = '1.0';
        to.style.transform = 'scale(1.0)';
    });
}

function animateFromSelectionToStats(): void {
    if (Util.getDeviceWidth() <= STATS_WIDTH_CHANGE) {
        animateFlexContainers(Div.selection, Div.stats);
    }
}

function animateFromStatsToSelection(): void {
    if (Util.getDeviceWidth() <= STATS_WIDTH_CHANGE) {
        if (ROTATIONS === 1) {
            rotateStats().then(() => {
                animateFlexContainers(Div.stats, Div.selection);
            });
        }
        else {
            animateFlexContainers(Div.stats, Div.selection);
        }
        document.querySelectorAll('.singleQuizChoice').forEach(el => {
            el.removeAttribute('selected');
        });
    }
}

function animateFromQuizToSelection(): void {
    if (CURRENT_QUIZ === null) {
        Util.redirect('/error');
        return;
    }
    const currQuizId = CURRENT_QUIZ.id;
    fetch(`/quiz/${currQuizId}/cancel`)
        .then(okResponseToJson).then(() => {
            animateFlexContainers(Div.gameContainer, Div.helloContainer);
            animateFromStatsToSelection();
            Time.waitHalfSecond().then(setupBegin);
        }, () => {
            Util.redirect('/error');
        }).catch(() => {
            Util.redirect('/error');
        });
}

function animateFromSaveToBegin(): void {
    animateFromStatsToSelection();
    Div.shadow.setAttribute('hidden', '');
    Div.resultsBoard.setAttribute('hidden', '');
    Time.waitSecond().then(() => {
        animateFlexContainers(Div.resultsContainer, Div.helloContainer);
        Time.waitHalfSecond().then(() => {
            Div.resultsContainer.style.opacity = '1.0';
            Div.resultsContainer.style.transform = 'scale(1.0)';
        }).then(setupBegin);
    });
}

function runGameForSelectedQuiz(): void {
    const currQuizId = parseInt(Div.startButton.getAttribute('runQuizId'), 10);
    fetch(`/quiz/${currQuizId}`)
        .then(okResponseToJson)
        .then((quizJson) => {
            if (!Quiz.isValidQuizFull(quizJson)) {
                throw Error('Invalid received data format');
            }
            CURRENT_QUIZ = quizJson;
            Time.waitSecond().then(() => {
                buildQuizWeb(CURRENT_QUIZ);
                animateFlexContainers(Div.helloContainer, Div.gameContainer);
                document.addEventListener('keydown', keyboardListener);
            });
        }, () => {
            Util.redirect('/error/alreadySolved');
        }).catch(() => {
            Util.redirect('/error/alreadySolved');
        });
}

function updateStatsData(stats: Quiz.QuizHistoryStats, id: number): void {
    createRanking(stats);
    if (stats.userStats === null) {
        Div.startButton.setAttribute('runQuizId', `${id}`);
        Div.startButton.removeAttribute('removed');
        Div.switchButton.setAttribute('removed', '');
        Time.waitShort().then(
            () => Div.startButton.removeAttribute('hidden'));
    }
    else {
        Div.switchButton.removeAttribute('removed');
        Div.startButton.setAttribute('removed', '');
        Time.waitShort().then(
            () => Div.switchButton.removeAttribute('hidden'));
    }
    animateFromSelectionToStats();
}

function addQuizSelectionListeners(): void {
    const selectors = document.querySelectorAll('.singleQuizChoice');
    selectors.forEach(el => {
        el.addEventListener('click', () => {
            if (el.hasAttribute('selected')) {
                el.removeAttribute('selected');
                if (ROTATIONS === 1) {
                    rotateStats();
                    Time.waitSecond().then(() => {
                        Util.queryDiv('#statsBoard').setAttribute('hidden', '');
                    });
                }
                else {
                    Util.queryDiv('#statsBoard').setAttribute('hidden', '');
                }
                Div.startButton.setAttribute('hidden', '');
                Div.switchButton.setAttribute('hidden', '');
                Time.waitHalfSecond().then(() => {
                    Div.startButton.setAttribute('removed', '');
                    Div.switchButton.setAttribute('removed', '');
                });
            }
            else {
                const id = parseInt(el.getAttribute('quizId'), 10);
                selectors.forEach(elem => elem.removeAttribute('selected'));
                el.setAttribute('selected', '');
                Util.removeChildrenOf(Util.queryDiv('#positions'));
                Util.removeChildrenOf(Util.queryDiv('#questionsResults'));
                fetch(`/quiz/${id}/stats`)
                    .then(okResponseToJson)
                    .then((quizHistory) => {
                        if (!Quiz.isValidQuizHistoryStats(quizHistory)) {
                            throw Error('Invalid received data format');
                        }
                        Util.queryDiv('#statsBoard').removeAttribute('hidden');
                        const stats: Quiz.QuizHistoryStats = quizHistory;
                        if (ROTATIONS === 1) {
                            rotateStats().then(() => updateStatsData(stats, id));
                        }
                        else {
                            updateStatsData(stats, id);
                        }
                    }, () => {
                        Util.redirect('/error')
                    }).catch(() => {
                        Util.redirect('/error');
                    });
            }
        });
    });
}

function addStatsDetailsListeners(): void {
    document.querySelectorAll('.statsPosition').forEach(el => {
        el.addEventListener('click', () => {
            const details = el.querySelector('.statsDetails') as HTMLDivElement;
            if (details.getAttribute('hidden') == null) {
                details.setAttribute('hidden', '');
            }
            else {
                details.removeAttribute('hidden');
            }
        });
    });
}

function addGameStats(results: boolean[], to: HTMLDivElement, times: number[],  validAnswers: number[]): void {
    results.forEach((valid, index) => {
        const answer = Util.createDivWithClasses('answerHistory', `${index + 1}`);
        if (valid) {
            answer.setAttribute('valid', 'true');
        }
        else {
            answer.setAttribute('valid', 'false');
        }
        if (times === null) {
            to.appendChild(answer);
        }
        else {
            const description = (validAnswers !== null && !valid)
                                ? ` (correct answer: ${validAnswers[index]})`
                                : '';
            const container = Util.createDivWithClasses('answerHistoryContainer');
            const timeQ = Util.createDivWithClasses('answerHistoryPenalty', `Full time: ${times[index]}s${description}`);
            container.appendChild(answer);
            container.appendChild(timeQ);
            to.appendChild(container);
        }
    });
}

function getTimeFormatted(resultS: number, fix: boolean = false): string {
    const seconds = fix ? (resultS % 60).toFixed(2) : (resultS % 60);
    if (resultS >= 60) {
        return `${Math.floor(resultS / 60)}m ${seconds}s`;
    }
    else {
        return `${seconds}s`;
    }
}

function addPositionList(resultQ: Quiz.BaseQuizHistoryStatistics, index: number): void {
    const position = Util.createDivWithClasses('statsPosition');
    position.classList.add('clickable');

    const id = Util.createDivWithClasses('statsId', `${index}.`);
    const time = resultQ.fullTime;
    const content = Util.createDivWithClasses('statsContent');
    const name = Util.createDivWithClasses('statsPointsName', `${resultQ.username} with time ${getTimeFormatted(time)}`);
    const details = Util.createDivWithClasses('statsDetails');
    details.setAttribute('hidden', '');

    addGameStats(resultQ.isAnswerValid, details, null, null);

    content.appendChild(name);
    content.appendChild(details);

    position.appendChild(id);
    position.appendChild(content);

    Util.queryDiv('#positions').appendChild(position);
}

function addQuestionPositionList(resultQ: Quiz.UserQuizHistoryStatistics, index: number): void {
    const isAnswerValid = resultQ.isAnswerValid[index];
    const validAnswer = resultQ.validAnswer[index];
    const answerAvgTime = resultQ.answerAvgTime[index];
    const position = Util.createDivWithClasses('statsPosition');

    const content = Util.createDivWithClasses('statsContentFull');
    const correct = Util.createDivWithClasses('statsPointsName', `Correct answer: ${validAnswer}`);
    const avg = Util.createDivWithClasses('statsPointsName', `Average time: ${getTimeFormatted(answerAvgTime, true)}`);

    content.appendChild(correct);
    content.appendChild(avg);

    const id = Util.createDivWithClasses('answerHistory', `${index + 1}`);
    id.setAttribute('valid', `${isAnswerValid}`);
    position.appendChild(id);
    position.appendChild(content);

    Util.queryDiv('#questionsResults').appendChild(position);
}

function addQuizListEntry(quizData: Quiz.Quiz, index: number): void {
    const choice = Util.createDivWithClasses('singleQuizChoice clickable');
    choice.setAttribute('quizId', `${quizData.id}`)
    const id = Util.createDivWithClasses('quizId', `${index}`);
    const name = Util.createDivWithClasses('quizName', quizData.title);

    choice.appendChild(id);
    choice.appendChild(name);
    Div.scrollQuiz.appendChild(choice);
}

function createRanking(results: Quiz.QuizHistoryStats): void {
    results.bestUsersStats.forEach((result, index) => {
        addPositionList(result, index + 1);
    });
    addStatsDetailsListeners();
    if (results.userStats === null) {
        return;
    }
    Util.queryDiv('#userTitle').textContent = `${results.userStats.username} result: ${getTimeFormatted(results.userStats.fullTime)}`;
    for (let index = 0; index < results.userStats.isAnswerValid.length; index++) {
        addQuestionPositionList(results.userStats, index);
    }
}

function okResponseToJson(response: Response): Promise<any> {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response.json();
}

function createQuizList(): void {
    Util.queryDiv('#statsBoard').setAttribute('hidden', '');
    Div.startButton.setAttribute('hidden', '');
    if (ROTATIONS === 1) {
        rotateStats();
    }
    Time.waitHalfSecond().then(() => Div.startButton.setAttribute('removed', ''));
    Util.markLoading(Div.scrollQuiz);
    fetch(`/quiz`)
        .then(okResponseToJson)
        .then((quizJson) => {
            if (!Quiz.isValidQuizList(quizJson)) {
                throw Error('Invalid received data format');
            }
            const data: Quiz.Quiz[] = quizJson;
            Time.waitSecond().then(() => {
                Util.removeChildrenOf(Div.scrollQuiz);
                data.forEach((quizData, index) => {
                    addQuizListEntry(quizData, index + 1);
                });
                CURRENT_QUIZ = null;
                addQuizSelectionListeners();
            });
        }, () => {
            Util.redirect('/error');
        }).catch(() => {
            Util.redirect('/error');
        });
}

// RESULT BOARD

function setupResultsBoard(): void {
    Time.stop();
    const quizId = CURRENT_QUIZ.id;
    const answers = ANSWERS;
    const questionsTimes = COUNT_TIMES.map(v => v.seconds);

    const fullResult = Quiz.getResultFor(quizId, answers, questionsTimes);

    fetch(`/quiz/${quizId}`, {
        credentials: 'same-origin',
        headers: {
            'Content-type': 'application/json',
            'CSRF-Token': CSRF_TOKEN
        },
        method: 'POST',
        body: JSON.stringify(fullResult)
    })
    .then(okResponseToJson)
    .then((jsonData) => {
        if (!Quiz.isValidGameStats(jsonData)) {
            throw Error('Invalid received data format');
        }
        const results: Quiz.GameStatistics = jsonData;
        const fullTime = results.answerFullTime.reduce((x, y) => x + y, 0);
        Div.result.textContent = getTimeFormatted(fullTime);
        Util.removeChildrenOf(Div.gameStats);
        Util.removeChildrenOf(Div.shadow);
        addGameStats(results.isAnswerValid, Div.gameStats, results.answerFullTime, results.validAnswer);
    }, () => {
        Util.redirect('/error');
    }).catch(() => {
        Util.redirect('/error');
    });
}

function goToGameStats(): void {
    document.removeEventListener('keydown', keyboardListener);
    Div.shadow.removeAttribute('hidden');
    Util.markLoading(Div.shadow);
    setupResultsBoard();

    Div.resultsContainer.style.display = 'flex';
    Time.waitShort().then(() => {
        Div.resultsBoard.removeAttribute('hidden');
    });
}

function rotateStats(): Promise<void> {
    const parent = Util.queryDiv('#statsBoard');
    parent.style.animation = 'none';
    const cloned = parent.cloneNode(true) as HTMLDivElement;
    cloned.childNodes[0].childNodes.forEach((node: HTMLDivElement) => {
        node.style.transform = 'rotateY(180deg)';
    });
    cloned.childNodes[1].childNodes.forEach((node: HTMLDivElement) => {
        node.style.transform = 'none';
    });
    cloned.append(...Array.from(cloned.childNodes).reverse());
    return Time.waitShort().then(() => {
        parent.style.animation = 'rotHalf 1s ease-in-out 1';
        ROTATIONS += 1;
        ROTATIONS %= 2;
        return Time.wait(1000).then(() => {
            parent.innerHTML = cloned.innerHTML;
        });
    });
}

// SETUP

Div.menuButton.addEventListener('click', toggleDescription);
Div.mainBackButton.addEventListener('click', animateFromStatsToSelection);
Div.startButton.addEventListener('click', runGameForSelectedQuiz);
Div.switchButton.addEventListener('click', rotateStats);
Div.finishButton.addEventListener('click', goToGameStats);
Div.goHome.addEventListener('click', animateFromSaveToBegin);
Div.closeButton.addEventListener('click', animateFromQuizToSelection);

setupBegin();
