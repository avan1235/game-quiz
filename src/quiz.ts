import { UserAnswerDB, UserQuizStatisticsDB } from './db';

export interface Question {
    id: number;
    title: string;
    body: string;
    possibleAnswers: number[];
    badAnswerPenaltyTime: number;
}

export interface QuestionFull extends Question {
    correctAnswerValue: number;
}

export interface Quiz {
    id: number;
    title: string;
}

export interface QuizFull extends Quiz {
    description: string;
    questions: Question[];
}

export interface QuizFullWithAnswers extends Quiz {
    description: string;
    questions: QuestionFull[];
}

export interface CountTime {
    seconds: number;
}

export interface Result {
    quizId: number;
    answers: number[];
    timesProportions: number[];
}

export interface GameStatistics {
    quizId: number;
    validAnswer: number[];
    isAnswerValid: boolean[];
    answerFullTime: number[];
}

export interface BaseQuizHistoryStatistics {
    username: string;
    fullTime: number;
    isAnswerValid: boolean[];
}

export interface UserQuizHistoryStatistics extends BaseQuizHistoryStatistics {
    validAnswer: number[];
    answerAvgTime: number[];
}

export interface QuizHistoryStats {
    userStats: UserQuizHistoryStatistics | null;
    bestUsersStats: BaseQuizHistoryStatistics[];
}

function isValidString(data: any): boolean {
    return ('string' === typeof data && data.length > 0);
}

function isIntegerNumber(data: any): boolean {
    return ('number' === typeof data && Number.isInteger(data));
}

function isNotEmptyArray(data: any): boolean {
    return (Array.isArray(data) && data.length > 0);
}

function isNotEmptyTypedArray(data: any, type: string): boolean {
    return (Array.isArray(data) && data.length > 0 && data.every(el => type === typeof el));
}

function isValidQuestion(jsonData: any): boolean {
    if ('object' !== typeof jsonData) {
        return false;
    }
    let hasTitle = false;
    let hasBody = false;

    for (const property in jsonData) {
        if (property === 'title') {
            if (isValidString(jsonData[property])) {
                hasTitle = true;
            }
            else {
                return false;
            }
        }
        else if (property === 'body') {
            if (isValidString(jsonData[property])) {
                hasBody = true;
            }
            else {
                return false;
            }
        }
        else if (property === 'possibleAnswers') {
            if (Array.isArray(jsonData[property])) {
                if (jsonData[property].length > 0) {
                    const answers = new Set();
                    for (const answer of jsonData[property]) {
                        if (!isIntegerNumber(answer) || answers.has(answer)) {
                            return false;
                        }
                        answers.add(answer);
                    }
                }
            }
            else {
                return false;
            }
        }
    }

    return hasTitle && hasBody;
}

function isValidQuizBase(jsonData: any): boolean {
    if ('object' !== typeof jsonData) {
        return false;
    }

    let hasTitle = false;
    let hasId = false;

    for (const property in jsonData) {
        if (property === 'title') {
            if (isValidString(jsonData[property])) {
                hasTitle = true;
            }
            else {
                return false;
            }
        }
        else if (property === 'id') {
            if (isIntegerNumber(jsonData[property])) {
                hasId = true;
            }
            else {
                return false;
            }
        }
    }
    return hasId && hasTitle;
}

export function checkQuizAnswers(validQuizData: QuizFullWithAnswers, userDataResult: any, userStartTime: number, username: string): [GameStatistics, UserAnswerDB[], UserQuizStatisticsDB] {
    if (userStartTime === undefined) {
        return undefined;
    }
    if (!isValidResult(userDataResult)) {
        return undefined;
    }
    const userData: Result = userDataResult;
    const correctAnswers = validQuizData.questions.map(q => q.correctAnswerValue);
    if (correctAnswers.length !== userData.answers.length
        || correctAnswers.length !== userData.timesProportions.length) {
            return undefined;
    }
    const questionResults = correctAnswers.map((v, i) => v === userData.answers[i]);
    const penaltiesTime = validQuizData.questions.map((q, i) => questionResults[i] ? 0 : q.badAnswerPenaltyTime);
    const playTimeS = (Date.now() - userStartTime) / 1000;
    const questionsTimes = userData.timesProportions.map(p => Math.floor(p / 100 * playTimeS));
    const questionsWithPenaltiesTimes = questionsTimes.map((t, i) => t + penaltiesTime[i]);
    const fullTime = questionsWithPenaltiesTimes.reduce((x, y) => x + y, 0);

    const quizStatsToSend: GameStatistics =  {
        quizId: validQuizData.id,
        validAnswer: correctAnswers,
        isAnswerValid: questionResults,
        answerFullTime: questionsWithPenaltiesTimes
    };

    const answers: UserAnswerDB[] = validQuizData.questions.map((q, i) => {
        return {
            question_id: q.id,
            answer_value: userData.answers[i],
            answered_by: username,
            answer_seconds: questionsWithPenaltiesTimes[i]
        }
    });

    const databaseStats: UserQuizStatisticsDB = {
        quiz_id: validQuizData.id,
        played_by: username,
        full_time: fullTime
    }

    return [quizStatsToSend, answers, databaseStats];
}

export function isValidGameStats(jsonData: any): boolean {
    if ('object' !== typeof jsonData) {
        return false;
    }
    let hasId = false;
    let hasAnswersResults = false;
    let hasAnswersTimes = false;
    let hasValidAnswers = false;
    for (const property in jsonData) {
        if (property === 'quizId') {
            if (isIntegerNumber(jsonData[property])) {
                hasId = true;
            }
            else {
                return false;
            }
        }
        else if (property === 'isAnswerValid') {
            if (isNotEmptyTypedArray(jsonData[property], 'boolean')) {
                hasAnswersResults = true;
            }
            else {
                return false;
            }
        }
        else if (property === 'validAnswer') {
            if (isNotEmptyTypedArray(jsonData[property], 'number')) {
                hasValidAnswers = true;
            }
            else {
                return false;
            }
        }
        else if (property === 'answerFullTime') {
            if (isNotEmptyTypedArray(jsonData[property], 'number')) {
                hasAnswersTimes = true;
            }
            else {
                return false;
            }
        }
    }
    const matchesLengths1 = jsonData.answerFullTime.length === jsonData.isAnswerValid.length;
    const matchesLengths2 = jsonData.answerFullTime.length === jsonData.validAnswer.length;
    return matchesLengths1 && matchesLengths2 && hasId
            && hasAnswersResults && hasAnswersTimes && hasValidAnswers;
}

export function isValidQuizList(jsonData: any): boolean {
    if (!Array.isArray(jsonData)) {
        return false;
    }
    let allValid = true;
    for (const quiz of jsonData) {
        allValid = allValid && isValidQuizBase(quiz);
    }
    return allValid;
}

export function isValidQuizFull(jsonData: any): boolean {
    if ('object' !== typeof jsonData) {
        return false;
    }
    let hasTitle = false;
    let hasDescription = false;
    let hasQuestions = false;
    let hasId = false;

    for (const property in jsonData) {
        if (property === 'title') {
            if (isValidString(jsonData[property])) {
                hasTitle = true;
            }
            else {
                return false;
            }
        }
        else if (property === 'description') {
            if (isValidString(jsonData[property])) {
                hasDescription = true;
            }
            else {
                return false;
            }
        }
        else if (property === 'id') {
            if (isIntegerNumber(jsonData[property])) {
                hasId = true;
            }
            else {
                return false;
            }
        }
        else if (property === 'questions') {
            if (isNotEmptyArray(jsonData[property])) {
                hasQuestions = true;
                for (const question of jsonData[property]) {
                    if (!isValidQuestion(question)) {
                        hasQuestions = false; // one of questions is invalid
                    }
                }
            }
            else {
                return false;
            }
        }
    }

    return hasId && hasTitle && hasDescription && hasQuestions;
}

function isValidBaseQuizHistoryStats(jsonData: any): boolean {
    if ('object' !== typeof jsonData) {
        return false;
    }
    let hasUsername = false;
    let hasFullTime = false;
    let hasAnswersResults = false;

    for (const property in jsonData) {
        if (property === 'username') {
            if (isValidString(jsonData[property])) {
                hasUsername = true;
            }
            else {
                return false;
            }
        }
        else if (property === 'fullTime') {
            if (isIntegerNumber(jsonData[property])) {
                hasFullTime = true;
            }
            else {
                return false;
            }
        }
        else if (property === 'isAnswerValid') {
            if (isNotEmptyTypedArray(jsonData[property], 'boolean')) {
                hasAnswersResults = true;
            }
            else {
                return false;
            }
        }
    }

    return hasUsername && hasFullTime && hasAnswersResults;
}

function isValidUserHistoryStats(jsonData: any): boolean {
    if (!isValidBaseQuizHistoryStats(jsonData)) {
        return false;
    }
    let hasValidAnswer = false;
    let hasAnswerAvgTime = false;
    for (const property in jsonData) {
        if (property === 'validAnswer') {
            if (isNotEmptyTypedArray(jsonData[property], 'number')) {
                hasValidAnswer = true;
            }
            else {
                return false;
            }
        }
        else if (property === 'answerAvgTime') {
            if (isNotEmptyTypedArray(jsonData[property], 'number')) {
                hasAnswerAvgTime = true;
            }
            else {
                return false;
            }
        }
    }

    return hasValidAnswer && hasAnswerAvgTime
            && jsonData.validAnswer.length === jsonData.answerAvgTime.length
            && jsonData.validAnswer.length === jsonData.answerAvgTime.length;
}

export function isValidQuizHistoryStats(jsonData: any): boolean {
    if ('object' !== typeof jsonData) {
        return false;
    }
    let hasUser = false;
    let hasBest = false;
    for (const property in jsonData) {
        if (property === 'userStats') {
            if (jsonData[property] === null || isValidUserHistoryStats(jsonData[property])) {
                hasUser = true;
            }
            else {
                return false;
            }
        }
        else if (property === 'bestUsersStats') {
            if (!Array.isArray(jsonData[property])) {
                return false;
            }
            for (const stat of jsonData[property]) {
                if (!isValidBaseQuizHistoryStats(stat)) {
                    return false;
                }
            }
            hasBest = true;
        }
    }

    return hasBest && hasUser;
}

function isValidResult(jsonData: any) {
    if ('object' !== typeof jsonData) {
        return false;
    }
    let hasId = false;
    let hasAnswers = false;
    let hasTimes = false;
    for (const property in jsonData) {
        if (property === 'quizId') {
            if (isIntegerNumber(jsonData[property])) {
                hasId = true;
            }
            else {
                return false;
            }
        }
        else if (property === 'answers') {
            if (isNotEmptyTypedArray(jsonData[property], 'number')) {
                hasAnswers = true;
            }
            else {
                return false;
            }
        }
        else if (property === 'timesProportions') {
            if (isNotEmptyTypedArray(jsonData[property], 'number')) {
                hasTimes = true;
            }
            else {
                return false;
            }
        }
    }
    const matchesLengths = jsonData.answers.length === jsonData.timesProportions.length;
    return matchesLengths && hasId && hasTimes && hasAnswers;
}

export function getResultFor(id: number,  answersValues: number[], questionsTimes: number[]): Result {
    const sum = questionsTimes.reduce((x, y) => x + y, 0);
    const proportion = questionsTimes.map(v => v / sum * 100.0);
    const percentSum = proportion.reduce((x, y) => x + y, 0);
    if (percentSum !== 100) {
        const differrence = percentSum - 100;
        let i = 0;
        let maxI = 0;
        while (i < proportion.length) {
            if (proportion[i] > proportion[maxI]) {
                maxI = i;
            }
            i += 1;
        }
        proportion[maxI] -= differrence;
    }
    return  {
        quizId: id,
        answers: answersValues,
        timesProportions: proportion
    };
}

export function isUserStats(stats: BaseQuizHistoryStatistics | UserQuizHistoryStatistics): stats is UserQuizHistoryStatistics {
    return (stats as UserQuizHistoryStatistics).validAnswer !== undefined;
}