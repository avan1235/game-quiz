import { CountTime } from "./quiz";

export interface TimeValue {
    seconds: number;
    minutes: number;
}

const currentTime: TimeValue = { minutes: 0, seconds: 0 };
let startedTime: boolean = false;
let incremented: CountTime = null;
let consumer: (time: TimeValue) => void = null;

export function stop(): TimeValue {
    startedTime = false;
    const stoppedTime: TimeValue = {
        seconds: currentTime.seconds,
        minutes: currentTime.minutes
    };
    return stoppedTime;
}

export function changeIncrementedTo(counter: CountTime): void {
    incremented = counter;
}

export function resetAndRun(timeValueConsumer: (time: TimeValue) => void): void {
    startedTime = false;
    currentTime.seconds = 0;
    currentTime.minutes = 0;
    consumer = timeValueConsumer;
    startedTime = true;
    incrementTimeAction();
}

function incrementTimeAction(): Promise<void> {
    const time = currentTime;
    const newTime = incrementTime(time);
    currentTime.minutes = newTime.minutes;
    currentTime.seconds = newTime.seconds;
    if (consumer !== null) {
        consumer(newTime);
    }
    if (startedTime) {
        if (incremented !== null) {
            incremented.seconds += 1;
        }
        return waitSecond().then(incrementTimeAction);
    }
}

export function format(time: TimeValue): string {
    const minutes = time.minutes.toString() + ':';
    const seconds = time.seconds < 10 ? '0' + time.seconds.toString() : time.seconds.toString();
    return minutes.concat(seconds);
}

function incrementTime(time: TimeValue): TimeValue {
    let newSeconds = time.seconds + 1;
    let newMinutes = time.minutes;
    if (newSeconds === 60) {
        newSeconds = 0;
        newMinutes += 1;
    }
    return { seconds: newSeconds, minutes: newMinutes };
}

export function wait(timeMillis: number): Promise<void> {
    return new Promise((resolve, _) => {
        window.setTimeout(resolve, timeMillis)
    });
}

export function waitSecond(): Promise<void> {
    return wait(1000);
}

export function waitHalfSecond(): Promise<void> {
    return wait(500);
}

export function waitQuarterSecond(): Promise<void> {
    return wait(250);
}

export function waitShort(): Promise<void> {
    return wait(50);
}