export function removeChildrenOf(el: HTMLDivElement): void {
    while (el.firstChild) {
        el.removeChild(el.lastChild);
    }
}

export function createDiv(): HTMLDivElement {
    return document.createElement('div');
}

export function redirect(location: string): void {
    window.location.replace(location);
}

export function addInputListener(input: HTMLInputElement): void {
    if (input === null) {
        return;
    }
    const container = input.parentElement;
    const label = container.querySelector('.inputLabel');

    ['focusin', 'input'].forEach(event => {
        input.addEventListener(event, () => {
            label.setAttribute('focused', '');
            container.setAttribute('focused', '');
        });
    });

    input.addEventListener('focusout', () => {
        if (input.value === '') {
            label.removeAttribute('focused');
        }
        container.removeAttribute('focused');
    });
}

export function createDivWithClasses(classesNames: string = null, textContent: string = null): HTMLDivElement {
    const divCreated = createDiv();
    if (classesNames !== null) {
        classesNames.split(' ').forEach(name => {
            divCreated.classList.add(name);
        });
    }
    if (textContent !== null) {
        divCreated.textContent = textContent;
    }
    return divCreated;
}

export function markLoading(el: HTMLDivElement): void {
    const loaderContainer = createDivWithClasses('loaderContainer');
    const loader = createDivWithClasses('loader');
    for (let i = 0; i < 4; i++) {
        const div = createDiv();
        loader.appendChild(div);
    }
    loaderContainer.appendChild(loader);
    removeChildrenOf(el);
    el.appendChild(loaderContainer);
}

export function queryDiv(name: string): HTMLDivElement {
    return document.querySelector(name);
}

export function getDeviceWidth(): number {
    return document.documentElement.clientWidth;
}

export function isWidthOverflown(elem: HTMLDivElement): boolean {
    return elem.scrollWidth > elem.clientWidth;
}

export function recreateNode(elem: HTMLDivElement): void {
    elem.parentNode.replaceChild(elem.cloneNode(true), elem);
}