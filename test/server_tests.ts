import 'mocha';
import { expect, assert } from 'chai';
import { driver, Key } from 'mocha-webdriver';
import { Server } from 'http';
import app from '../dist/app';

const TEST_PORT = 8080;
const SERVER_TEST_PATH = `http://localhost:${TEST_PORT}`;
const ANIM_TIMEOUT = 1000;
const TIMEOUT = 60000;
const SAMPLE_LOGIN = 'user0';
const SAMPLE_PASSWORD = 'user0';
const SESSION_KEY = 'connect.sid';
const SOLVED_QUESTIONS = 9;
const SOLVED_QUIZ_ID = 3;

let server: Server = null;


describe('server tests for logging and path getting', function () {
    this.timeout(TIMEOUT);

    beforeEach(done => {
        server = app.listen(TEST_PORT, done);
    });

    afterEach(done => {
        if (server !== null) {
            server.close(done);
            server = null;
        }
    });

    it('should redirect to login page when not logged in', async () => {
        await clearSession();
        await driver.get(SERVER_TEST_PATH);
        await checkPathFor('/auth');
    });

    it('should redirect to game when logged in', async () => {
        await clearSession();
        await driver.get(SERVER_TEST_PATH);

        // login user
        await loginUser();
        await checkPathFor('/');
    });

    it('should show error on not existing pages', async () => {
        const testedPaths = ['/error', '/login', '/game', '/start'];
        for (const path of testedPaths) {
            await driver.get(`${SERVER_TEST_PATH}${path}`);
            const elements = await driver.findAll('#errorTitle');
            expect(elements.length).to.equal(1);
        };
    });

    it('should not allow play for user when password changed (checked on page refresh)', async () => {
        await clearSession();
        await driver.get(`${SERVER_TEST_PATH}/auth`);
        // login user
        await loginUser();
        const cookie = await driver.manage().getCookie(SESSION_KEY);
        await checkPathFor('/');
        await clearSession();
        await driver.get(`${SERVER_TEST_PATH}`);
        await checkPathFor('/auth');

        // change password
        await driver.find('#resetPasswordButton').doClick();
        await checkPathFor('/reset');
        await changeUserPasswordTo(SAMPLE_PASSWORD, SAMPLE_PASSWORD);

        await checkPathFor('/auth');
        await driver.manage().addCookie({ name: SESSION_KEY, value: cookie.value });

        // try to go to game with old cookies
        await driver.get(`${SERVER_TEST_PATH}`);
        // should redirect to login page
        await checkPathFor('/auth');
    });

    it('should should give info when quiz started already by user', async () => {
        await clearSession();
        await driver.get(`${SERVER_TEST_PATH}/auth`);
        // login user
        await loginUser();
        await checkPathFor('/');
        const quizPath = `${SERVER_TEST_PATH}/quiz/${SOLVED_QUIZ_ID}`;
        await driver.get(quizPath);
        await driver.get(quizPath);
        expect(await driver.find('#errorDescription').getText()).to.include('Quiz started but not canceled');
        await driver.get(`${quizPath}/cancel`);
        await driver.get(quizPath);
        // now there should be no errors
        try {
            await driver.find('#errorDescription');
            assert(false, 'No errors expected after cancelig quiz');
        } catch (e) {
            assert(true);
        }
        await driver.get(`${quizPath}/cancel`); // cancel started quiz
    });

    it('should should allow to cancel quiz in interface by user', async () => {
        await clearSession();
        await driver.get(`${SERVER_TEST_PATH}/auth`);
        // login user
        await loginUser();
        await checkPathFor('/');
        await sleep(ANIM_TIMEOUT);
        await driver.findWait(`div.singleQuizChoice:nth-child(${SOLVED_QUIZ_ID})`, ANIM_TIMEOUT).doClick();
        await driver.findWait('#start', ANIM_TIMEOUT).doClick();
        await sleep(3000);
        await driver.findWait('#close', ANIM_TIMEOUT).doClick();
        await checkPathFor('/');
        await sleep(ANIM_TIMEOUT);
        await driver.findWait(`div.singleQuizChoice:nth-child(${SOLVED_QUIZ_ID})`, ANIM_TIMEOUT).doClick();
        await driver.findWait('#start', ANIM_TIMEOUT).doClick();
        await sleep(3000);
        // cancel it again not be opened for sure
        await driver.findWait('#close', ANIM_TIMEOUT).doClick();
    });

    it('should allow to solve the quiz once', async () => {
        await clearSession();
        await driver.get(`${SERVER_TEST_PATH}/auth`);
        // login user
        await loginUser();
        await checkPathFor('/');

        await driver.findWait(`div.singleQuizChoice:nth-child(${SOLVED_QUIZ_ID})`, TIMEOUT).doClick();
        await driver.findWait('#start', TIMEOUT).doClick();
        await sleep(2000);
        for (let i = 0; i < SOLVED_QUESTIONS; i++) {
            await sleep(800);
            await driver.sendKeys(Key.ARROW_DOWN);
            await sleep(100);
            await driver.sendKeys('42');
            await sleep(100);
            await driver.sendKeys(Key.ARROW_RIGHT);
        }
        await sleep(800);
        await driver.findWait('#finishButton', TIMEOUT).doClick();
        await driver.findWait('#goHome', TIMEOUT).doClick();
        await sleep(2000);
        // quiz fully solved, try to run it again and expect error
        await driver.findWait('div.singleQuizChoice:nth-child(3)', TIMEOUT).doClick();
        // start button should not be visible when quiz was already solved
        try {
            await driver.findWait('#start', TIMEOUT);
            assert(false, 'Start button should not be found');
        } catch (e) {
            // passed
            assert(true);
        }
    });

    it('should share only base quiz data under /quiz and details under /quiz/id', async () => {
        await clearSession();
        // login user
        await driver.get(`${SERVER_TEST_PATH}/auth`);
        await loginUser();
        await checkPathFor('/');
        // get all quiz lists and check for details
        const details = ['question', 'penalty', 'answers'];

        // get specific quiz by id with details
        await driver.get(`${SERVER_TEST_PATH}/quiz/1`);
        const bodyDetailed = await driver.getPageSource();
        for (const d of details) {
            expect(bodyDetailed.toLowerCase()).to.include(d);
        }

        // cancel quiz at finish
        await driver.get(`${SERVER_TEST_PATH}/quiz`);
        const body = await driver.getPageSource();
        for (const d of details) {
            expect(body.toLowerCase()).not.to.include(d);
        }
    });

    it('should show only quiz list after logging', async () => {
        await clearSession();
        await driver.get(`${SERVER_TEST_PATH}/auth`);
        // login user
        await loginUser();
        await sleep(2000);
        const quizes = await driver.findAll('.singleQuizChoice');
        expect(quizes.length).not.to.equal(0);
        try {
            await driver.findAll('questionBody');
            assert(false, 'There shoud be no questions when quiz not selected');
        }
        catch (err) {
            assert(true);
        }
    });

    it('should show not quiz list but question after selecting quiz', async () => {
        await clearSession();
        await driver.get(`${SERVER_TEST_PATH}/auth`);
        // login user
        await loginUser();
        await sleep(ANIM_TIMEOUT);
        await driver.findWait(`div.singleQuizChoice:nth-child(2)`, ANIM_TIMEOUT).doClick();
        await driver.findWait('#start', ANIM_TIMEOUT).doClick();
        await sleep(2000);
        const question = await driver.findAll('.questionBody');
        expect(question.length).not.to.equal(0);
        try {
            await driver.findAll('singleQuizChoice');
            assert(false, 'There should be no quiz list when solving quiz');
        }
        catch (err) {
            assert(true);
        }
        await driver.findWait('#close', ANIM_TIMEOUT).doClick();
    });
});

async function checkPathFor(path: string): Promise<void> {
    const url = await driver.getCurrentUrl();
    const result = url.endsWith(path);
    expect(result).to.equal(true, `${url} should end with ${path}`);
}

async function clearSession(): Promise<void> {
    return driver.manage().deleteCookie(SESSION_KEY);
}

async function loginUser(): Promise<void> {
    await driver.find('#nameInput').sendKeys(SAMPLE_LOGIN);
    await driver.find('#passwordInput').sendKeys(SAMPLE_PASSWORD);
    await driver.find('#login').doClick();
}

async function changeUserPasswordTo(password: string, oldPassword: string): Promise<void> {
    await driver.find('#nameInput').sendKeys(SAMPLE_LOGIN);
    await driver.find('#passwordInput').sendKeys(SAMPLE_PASSWORD);
    await driver.find('#newPasswordInput').sendKeys(password);
    await driver.find('#reset').doClick();
}

function sleep(ms: number): Promise<void> {
    return new Promise(resolve => setTimeout(resolve, ms));
}